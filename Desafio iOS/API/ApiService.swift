//
//  ApiService.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import Foundation
import Alamofire

class ApiService {
    
    func get(url: String, completion: @escaping (Data?) -> Void) {
        Alamofire.request(url)
            .responseString { response in
                let result = "\(response.result.value!)"
                completion(result.data(using: .utf8))
        }
    }
    
}
