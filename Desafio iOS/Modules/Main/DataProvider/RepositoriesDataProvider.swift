//
//  MainDataProvider.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import Foundation

class RepositoriesDataProvider: DataProviderManager<RepositoriesDataProviderProtocol, Any> {
    
    let apiService: ApiService = ApiService()
    
    func sendRepositoriesRequest(page: Int, isRefresh: Bool) {
        let url = self.getEndpoint(name: "EndpointForRepositories").replacingOccurrences(of: "{page}", with: String(page))
        self.apiService.get(url: url, completion: { (data) in
            do {
                let response = try JSONDecoder().decode(RepositoryResponse.self, from: data!)
                self.dataSourceDelegate?.getRepositoriesResponse(repositories: response.items!, isRefresh: isRefresh)
            } catch {
                let response = try! JSONDecoder().decode(Error.self, from: data!)
                self.dataSourceDelegate?.errorData(message: response.message!)
            }
        })
    }
    
}

protocol RepositoriesDataProviderProtocol: GenericDataProvider {
    func getRepositoriesResponse(repositories: [Repository], isRefresh: Bool)
}
