//
//  PullsDataProvider.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import Foundation

class PullsDataProvider: DataProviderManager<PullsDataProviderProtocol, Any> {
    
    let apiService: ApiService = ApiService()
    
    func sendPullsRequest(repository: Repository) {
        let url = self.getEndpoint(name: "EndpointForPulls")
            .replacingOccurrences(of: "{login}", with: repository.owner?.login ?? "")
            .replacingOccurrences(of: "{repo}", with: repository.name ?? "")
        self.apiService.get(url: url, completion: { (data) in
            do {
                let list = try JSONDecoder().decode([Pull].self, from: data!)
                self.dataSourceDelegate?.getPullsResponse(pulls: list)
            } catch {
                let response = try! JSONDecoder().decode(Error.self, from: data!)
                self.dataSourceDelegate?.errorData(message: response.message!)
            }
        })
    }
    
}

protocol PullsDataProviderProtocol: GenericDataProvider {
    func getPullsResponse(pulls: [Pull])
}
