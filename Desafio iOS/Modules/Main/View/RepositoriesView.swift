//
//  RepositoriesView.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import UIKit

class RepositoriesView: UIView {

    @IBOutlet weak var tableView: UITableView!

    override func awakeFromNib() {
        self.tableView.register(UINib(nibName: "RepositoryCell", bundle: nil), forCellReuseIdentifier: "RepositoryCell")
        self.tableView.rowHeight = 130
    }
}
