//
//  Owner.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import Foundation

class User: Codable {
    var id: Int?
    var login: String?
    var avatarUrl: URL?
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: UserKeys.self)
        id = try values.decode(Int?.self, forKey: .id)
        login = try values.decode(String?.self, forKey: .login)
        avatarUrl = try values.decode(URL?.self, forKey: .avatarUrl)
    }
    
    enum UserKeys: String, CodingKey {
        case id = "id"
        case login = "login"
        case avatarUrl = "avatar_url"
    }
}
