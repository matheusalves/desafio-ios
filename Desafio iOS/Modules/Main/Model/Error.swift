//
//  Generic.swift
//  Desafio iOS
//
//  Created by Matheus Alves Mendonça on 22/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import Foundation

class Error: Codable {
    var message: String?
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: ErrorKeys.self)
        message = try values.decode(String?.self, forKey: .message)
    }
    
    enum ErrorKeys: String, CodingKey {
        case message = "message"
    }
}
