//
//  Pull.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import Foundation

class Pull: Codable {
    var id: Int?
    var title: String?
    var body: String?
    var url: URL?
    var date: String?
    var user: User?
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: PullKeys.self)
        id = try values.decode(Int?.self, forKey: .id)
        title = try values.decode(String?.self, forKey: .title)
        body = try values.decode(String?.self, forKey: .body)
        url = try values.decode(URL?.self, forKey: .url)
        date = try values.decode(String?.self, forKey: .date)
        date = String(describing: date?.prefix(10))
        user = try values.decode(User?.self, forKey: .user)
    }
    
    enum PullKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case body = "body"
        case url = "html_url"
        case date = "created_at"
        case user = "user"
    }
}
