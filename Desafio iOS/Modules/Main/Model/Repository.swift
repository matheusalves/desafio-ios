//
//  Repository.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import Foundation

class Repository: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var forks: Int?
    var stars: Int?
    var owner: User?
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: RepositoryKeys.self)
        id = try values.decode(Int?.self, forKey: .id)
        name = try values.decode(String?.self, forKey: .name)
        description = try values.decode(String?.self, forKey: .description)
        forks = try values.decode(Int?.self, forKey: .forks)
        stars = try values.decode(Int?.self, forKey: .stars)
        owner = try values.decode(User?.self, forKey: .owner)
    }
    
    enum RepositoryKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case description = "description"
        case forks = "forks_count"
        case stars = "stargazers_count"
        case owner = "owner"
    }
}

class RepositoryResponse: Codable {
    var items: [Repository]?
    
    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: RepositoryResponseKeys.self)
        items = try values.decode([Repository]?.self, forKey: .items)
    }
    
    enum RepositoryResponseKeys: String, CodingKey {
        case items = "items"
    }
}
