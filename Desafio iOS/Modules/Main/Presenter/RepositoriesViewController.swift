//
//  ViewController.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import UIKit
import SDWebImage

class RepositoriesViewController: BaseViewController {
    
    let dataProviderManager: RepositoriesDataProvider = RepositoriesDataProvider()
    var repositories: [Repository] = []
    var currentPage: Int = 1

    var mainView: RepositoriesView {
        get {
            return self.view as! RepositoriesView
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataProviderManager.dataSourceDelegate = self
        self.showLoading()
        self.dataProviderManager.sendRepositoriesRequest(page: currentPage, isRefresh: false)
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.mainView.tableView.addSubview(refreshControl)
    }
    
    @objc func refresh() {
        self.showLoading()
        self.currentPage = 1
        self.dataProviderManager.sendRepositoriesRequest(page: 1, isRefresh: true)
    }

}

extension RepositoriesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: RepositoryCell? = tableView.dequeueReusableCell(withIdentifier: "RepositoryCell", for: indexPath) as? RepositoryCell
        cell?.nameLabel.text = self.repositories[indexPath.row].name
        cell?.descriptionLabel.text = self.repositories[indexPath.row].description
        cell?.forksLabel.text = "\(self.repositories[indexPath.row].forks!)"
        cell?.starsLabel.text = "\(self.repositories[indexPath.row].stars!)"
        cell?.usernameLabel.text = self.repositories[indexPath.row].owner?.login
        cell?.avatarImageView.sd_setImage(with: self.repositories[indexPath.row].owner?.avatarUrl, placeholderImage: UIImage(named: "no-photo"))
        return cell ?? RepositoryCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc: PullsViewController? = self.storyboard?.instantiateViewController(withIdentifier: "PullsViewController") as? PullsViewController
        if let viewController = vc {
            viewController.repository = self.repositories[indexPath.row]
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == self.repositories.count - 1 {
            self.showLoading()
            self.currentPage += 1
            self.dataProviderManager.sendRepositoriesRequest(page: currentPage, isRefresh: false)
            self.mainView.tableView.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
    
}

extension RepositoriesViewController: RepositoriesDataProviderProtocol {
    func getRepositoriesResponse(repositories: [Repository], isRefresh: Bool) {
        self.refreshControl.endRefreshing()
        self.dismissLoading()
        if isRefresh { self.repositories = [] }
        self.repositories.append(contentsOf: repositories)
        self.mainView.tableView.reloadData()
    }
    
    func errorData(message: String) {
        self.dismissLoading()
        self.showAlert(title: "Erro", message: message)
    }
}
