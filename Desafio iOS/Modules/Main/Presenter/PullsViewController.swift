//
//  PullsViewController.swift
//  Desafio iOS
//
//  Created by Matheus Alves on 21/09/17.
//  Copyright © 2017 Matheus Alves. All rights reserved.
//

import UIKit

class PullsViewController: BaseViewController {
    
    let dataProviderManager: PullsDataProvider = PullsDataProvider()
    var repository: Repository?
    var pulls: [Pull] = []
    
    var mainView: PullsView {
        get {
            return self.view as! PullsView
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = self.repository?.name
        
        self.dataProviderManager.dataSourceDelegate = self
        self.showLoading()
        self.dataProviderManager.sendPullsRequest(repository: self.repository!)
        
        self.refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.mainView.tableView.addSubview(refreshControl)
    }

    @objc func refresh() {
        self.showLoading()
        self.dataProviderManager.sendPullsRequest(repository: self.repository!)
    }

}

extension PullsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pulls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PullCell? = tableView.dequeueReusableCell(withIdentifier: "PullCell", for: indexPath) as? PullCell
        cell?.titleLabel.text = self.pulls[indexPath.row].title
        cell?.bodyLabel.text = self.pulls[indexPath.row].body
        cell?.usernameLabel.text = self.pulls[indexPath.row].user?.login
        cell?.avatarImageView.sd_setImage(with: self.pulls[indexPath.row].user?.avatarUrl, placeholderImage: UIImage(named: "no-photo"))
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatter.date(from: self.pulls[indexPath.row].date!) {
            dateFormatter.dateFormat = "dd-MM-yyy"
            cell?.dateLabel.text = dateFormatter.string(from: date)
        }
        return cell ?? RepositoryCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        UIApplication.shared.openURL(self.pulls[indexPath.row].url!)
    }
    
}

extension PullsViewController: PullsDataProviderProtocol {
    func getPullsResponse(pulls: [Pull]) {
        self.refreshControl.endRefreshing()
        self.dismissLoading()
        self.pulls = pulls
        self.mainView.tableView.reloadData()
    }
    
    func errorData(message: String) {
        self.dismissLoading()
        self.showAlert(title: "Erro", message: message)
    }
}
